# README #

This module should be installed the usual way for any Drupal module.

### What is this repository for? ###

This is a Drupal module which retrieves moon phases from the Astronomical Applications API (http://aa.usno.navy.mil/data/docs/api.php) and displays the next new moon.

An example call to the API is as follows: `http://api.usno.navy.mil/moon/phase?date=06/15/2017&nump=4`, which returns the following result:

```
{
      "error":false,
      "apiversion":"2.0.0",
      "year":2017,
      "month":6,
      "day":15,
      "numphases":4,
      "datechanged":false, 
      "phasedata":[
            {
               "phase":"Last Quarter",
               "date":"2017 Jun 17",
               "time":"11:33"
            },
            {
               "phase":"New Moon",
               "date":"2017 Jun 24",
               "time":"02:31"
            },
            {
               "phase":"First Quarter",
               "date":"2017 Jul 01",
               "time":"00:51"
            },
            {
               "phase":"Full Moon",
               "date":"2017 Jul 09",
               "time":"04:07"
            }
  ]
}
```